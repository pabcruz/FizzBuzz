package cl.ubb.fizzbuzztest;

import static org.junit.Assert.*;
import org.junit.Test;
import cl.ubb.FizzBuzz.FizzBuzz;

public class FizzBuzzTest {

	@Test
	public void validarRecibeUnoDevuelveUno() {
		//Arrange 
				FizzBuzz FizzBuzz=new FizzBuzz();
				String  resultado;
		//Act
				resultado=FizzBuzz.solucionFizzBuzz(1);
				
		//Assert
				assertEquals(resultado,"1");
	}
	
	@Test
	public void validarRecibeDosDevuelveDos() {
		//Arrange 
				FizzBuzz FizzBuzz=new FizzBuzz();
				String  resultado;
		//Act
				resultado=FizzBuzz.solucionFizzBuzz(2);
				
		//Assert
				assertEquals(resultado,"2");
	}
	
	@Test
	public void validarRecibeTresDevuelveFizz() {
		//Arrange 
				FizzBuzz FizzBuzz=new FizzBuzz();
				String  resultado;
		//Act
				resultado=FizzBuzz.solucionFizzBuzz(3);
		//Assert
				assertEquals(resultado,"Fizz");
	}
	
	@Test
	public void validarRecibeCuatroDevuelveCuatro() {
		//Arrange 
				FizzBuzz FizzBuzz=new FizzBuzz();
				String  resultado;
		//Act
				resultado=FizzBuzz.solucionFizzBuzz(4);
		//Assert
				assertEquals(resultado,"4");
				
	}
	
	@Test
	public void validarRecibeCincoDevuelveBuzz() {
		//Arrange 
				FizzBuzz FizzBuzz=new FizzBuzz();
				String  resultado;
		//Act
				resultado=FizzBuzz.solucionFizzBuzz(5);
		//Assert
				assertEquals(resultado,"Buzz");
				
	}
	
	@Test
	public void validarRecibeQuinceDevuelveFizzBuzz() {
		//Arrange 
				FizzBuzz FizzBuzz=new FizzBuzz();
				String  resultado;
		//Act
				resultado=FizzBuzz.solucionFizzBuzz(15);
		//Assert
				assertEquals(resultado,"FizzBuzz");
				
	}
	
	
	
	

}
